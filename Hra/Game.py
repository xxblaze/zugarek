import os
import math
import random

import Zbrane
import Enemy
from Stuff import *

# hlavní smyčka hry je na řádku 936
# každá lokace vrací název další lokace k zobrazení

taktika = ["prvni", "druhy"]
prvni_druhy = ["prvni", "druhy"]
klub_prestrelka_probehla = False
utok = ["krk", "břicho", "ruka"]
prestrelka_rozhodovani = ["zdrhnout", "schovka"]
nuz_revolver = ["nuz", "revolver"]
prava_leva = ["prava", "leva"]
inv = Inventar()
ano_ne = ["ano", "ne"]
inventar_rozhodovani = ["nuz a penize", "penize a kokain"]
klub_oblecení = ["klub", "obleceni"]
barman_piti = ["kořalka", "rum", "ochucená voda"]
jaky_obchod = ["obleceni", "klub"]
druhy_obleku = ["Cerny", "CernyPruhy", "CernyKostkovany", "Hnedy", "TmavaModra", "TmavaModraPruhy", "konec"]
prvni_utok_bez_noze = ["ano"," ne"]
druhy_zbrani = ["thompson", "nuz", "lewis", "revolver", "palka", "kamen", "konec"]
obchod_check = False
bar_check = False
Ammunation_check = False
oblek_check = False

name = None

zivot = 100
naboje_pistol = 0
naboje_samopal = 0
penize = 0
kokain = 0
Thompson = 0
nuz = 0
smer = ["leva, prava, dopredu, zpatky"]

str_nuz = "nůž"
str_penize = "peníze"

def cls():
    pass
    # os.system('cls')


def pressKey():
    input("zmáčkni jakoukoli klávesu pro pokračování")

def uvod():

    global name
    print("𝓶𝓪𝓭𝓮 𝓫𝔂 𝔁𝓑𝓵𝓪𝔃𝓮")
    print("Ahoj, já jsem vypravěč který tě vezme do dvacátých let minulého století")
    name = input("Nejdřív mi ale řekni své jméno: ")
    print("Dobrá " + name + " doufám že máš silný žaludek protože tohle bude drsná jízda")
    print("\n")

    print("game tag")
    print("𝓹𝓪𝓲𝓷, 𝓼𝓾𝓯𝓯𝓮𝓻𝓲𝓷𝓰 𝓪𝓷𝓭 𝓮𝓼𝓹𝓮𝓬𝓲𝓪𝓵𝓵𝔂 𝓵𝓲𝓯𝓮")
    print("\n")
    pressKey()
    cls()
    print("""Dobře se ale rozhodni jestli Jsi připraven
    vztoupit do tohoto nikotinem a dehtem zapuchlého místa
    jsi opravdu připravený? jelikož toto místo už nenavštívil dlouho i sám bůh
    pokud ano nech mě abych tě zde provedl""")
    print("\n")
    input("zmáčkni jakoukoli klávesu pro pokračování")
    return predmesti

def start():

    while odpoved not in prvni_utok_bez_nože:

         print("vstaneš z pod stolu a vidíš jak ti muž míří zbraní na hlavu)")
         print("-chceš na něj zaútočit nebo ne?")
         odpoved = input("\nano/ne")
         if odpoved == "ano":
             print("bohužel nemáš u sebe žádnou zbraň jelikož jsi si od vpravěče vzal peníze")
             print("muž ani nemrkne a střelí tě do hlavy ")
             print("kulka ti proleťela skrz hlavu je mi líto ale zemřel jsi")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")


         elif odpoved == "ne":
             print("muž ani nemrkne a střelí tě do hlavy ")
             print("kulka ti proleťela skrz hlavu je mi líto ale zemřel jsi")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")

             odpoved = ""
         else:
             print("špatná odpověď nestihl jsi zareagovat a muž tě střelil do hlavy\n")
             print("kulka ti proleťela skrz hlavu je mi líto ale zemřel jsi")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")




def placeni(cena):
    global penize
    if penize >= cena:
        penize = penize - cena
        print("transakce byla úspěšná")
        return True

    else:
        print("je mi líto ale na tuto položku nemáte dostatečné pěněžní zdroje")
        return False





def Ammunition():

    Ammunation_check = True

    print("v obchodě se zbraňemi vidíš svalnatého muže az pultem a spoustu zbraní")

    odpoved = ""
    while odpoved != "konec":
        print("""hele podívej se mám tady spoustu zbraní
každná má jiný poškození tak na to mysli než si koupíš tu nejlevnější
vyšší cena lepší poškození""")
        print("""mám tady tenhle zbrusu nový samopal Thompson tahle kráska dává poškození 18 stojí okolo 40 šilinků
také je zde tento klasický nůž který má poškození 6 ten tě bude stát 7 šilinků
je zde také Lewis Machine gun podívej tuhle krásku tady nemám úplně legálně má poškození 45 a dám ti ji jenom za 50 šilinků
nebo je tady další klasika Revolver který má poškození 9 a bude to jenom 15 šilinků
nebo tahle baseballová pálka s poškozením 4 a stojí 6 šilinky
a potom něco pro největší spodinu zde mám kameny ty stojí 1 šilink

pro ukončení nákupu- konec""")


        odpoved = input("\nthompson/nuz/lewis/revolver/palka/kamen\n")
        if odpoved == "thompson":
            if placeni(40):
                print("Muž zajde dozadu do skladu a donese ti tvoji zbraň")


        elif odpoved == "nuz":
            if placeni(7):
                print("Muž zajde dozadu do skladu a donese ti tvoji zbraň")


        elif odpoved == "lewis":
            if placeni(50):
                print("Muž zajde dozadu do skladu a donese ti tvoji zbraň")



        elif odpoved == "revolver":
            if placeni(15):
                print("Muž zajde dozadu do skladu a donese ti tvoji zbraň")


        elif odpoved == "palka":
            if placeni(6):
                print("Muž zajde dozadu do skladu a donese ti tvoji zbraň")



        elif odpoved == "kamen":
            if placeni(1):
                print("Muž zajde dozadu do skladu a donese ti tvoji zbraň")

        elif odpoved == "konec":

            print("naschle pane")
            break
        else:
            print("Promiň ale to jsem nepochopil. Řekni ti to Ještě jednou\n")
    return namesti


def potraviny():
    obchod_check = True
    print("v obchodě s potravinami vidíš množství chutného jídla")
    print("blíží se k tobě milá paní")

    odpoved = ""
    while odpoved != "konec":
        print("""čím Vás mohu obloužit ctěný pane?""")
        print()


        odpoved = input("""
        - obložený chlebíček
        - kuře
        - zásoby na cesty
        - těstoviny
        - brambory
        - cigarety
        - tabák
        - rýži
        - dort
        pro ukončení nákupu- konec""")
        if odpoved == "obložený chlebíček":
            if placeni(40):
                print("paní ti položku namarkuje a zabalí")

        elif odpoved == "kuře":
            if placeni(7):
                print("paní ti položku namarkuje a zabalí")


        elif odpoved == "zásoby na cesty":
            if placeni(50):
                print("paní ti položku namarkuje a zabalí")

        elif odpoved == "těstoviny":
            if placeni(15):
                print("paní ti položku namarkuje a zabalí")


        elif odpoved == "brambory":
            if placeni(6):
                print("paní ti položku namarkuje a zabalí")

        elif odpoved == "cigarety":
            if placeni(1):
                print("paní ti položku namarkuje a zabalí")

        elif odpoved == "konec":

            break

        elif odpoved == "rýži":
            if placeni(1):
                print("paní ti položku namarkuje a zabalí")

        elif odpoved == "dort":
            if placeni(1):
                print("paní ti položku namarkuje a zabalí")
        else:
            print("Promiňte, špatně jsem vám rozumněla. Řekni te to prosím ještě jednou\n")
    return namesti


def ano_oblek():
    print("vidíš jak se k tobě blíží malý srandovní mužíček a ptá se tě ")

    odpoved = ""

    while odpoved not in druhy_obleku:
        print("")
        print("který oblek ti můžu z naší nabídky nabídnout?")
        print("tento krásný čistě černý oblek s kloboukem zdarma- 5 šilinků")
        print("nebo tento černý s táhlímy pruhy od vrchu až k nohám- 15")
        print("ale na váš tipuju tenhle černý kostkovaný ten sedí k vaším očím- 7")
        print("také musím podotknout že je zde tento čistě hnědý krasavec- 5")
        print("také je zde tenhle který nese název Royal Blue ale musím podotknout že jeho cenna je trochu větší než u ostatních- 45")
        print("Je zde i tato levnější varianta Royal Blue with stripes ale ten není tak unikátní- 30")
        print("")
        print("pro ukončení nákupu- konec")

        odpoved = input("\nCerny/CernyPruhy/CernyKostkovany/Hnedy/TmavaModra/TmavaModraPruhy\n")
        if odpoved == "Cerny":

            if placeni(5):
                print("*Mužíček se sradostí rozběhne pro černý oblek a hned ti ho namarkuje*")
                inv.pridatDoInventare("cerny_oblek")
            odpoved = None
            pressKey()

        elif odpoved == "CernyPruh":

            if placeni(15):
                print("*Mužíček se sradostí rozběhne pro černý oblek s pruhy a hned ti ho namarkuje*")
                inv.pridatDoInventare("cerny_oblek_pruhy")
            odpoved = None


        elif odpoved == "CernyKostkovany":

            if placeni(7):
                print("*Mužíček se sradostí rozběhne pro černý oblek s kostkovným vzorem a hned ti ho namarkuje*")
                inv.pridatDoInventare("cerny_oblek_kostky")
            odpoved = None


        elif odpoved == "CernyKostkovany":

            if placeni(5):
                print("*Mužíček se sradostí rozběhne pro hnědý oblek a hned ti ho namarkuje*")
                inv.pridatDoInventare("hnedy_oblek")
            odpoved = None


        elif odpoved == "TmavaModra":

            if placeni(45):
                print("*Mužíček se sradostí rozběhne pro tmavě modrý oblek a hned ti ho namarkuje*")
                inv.pridatDoInventare("tmavy_modry_oblek")
            odpoved = None


        elif odpoved == "TmavaModraPruhy":

            if placeni(30):
                print("*Mužíček se sradostí rozběhne pro tmavý oblek s pruhovaným vzorem a hned ti ho namarkuje*")
                inv.pridatDoInventare("tmavy_oblek")
            odpoved = None

        elif odpoved == "konec":

            print("naschle pane")
            return namesti()


            odpoved = ""
        else:
            print("Promiň ale to jsem nepochopil. Řeknu ti to Ještě jednou\n")

    def ne_oblek():
        print("proč marníte můj čas?")
        pass

# pokud hráč stiskne určitou klávesu zobrazí se mu inventář tuto akci může použít kdykoliv ve hře kromě boje
    def ukazat_inventar():
        pass



def odev():
    print("*ocitnul jses v obchodu s oblečením*")
    print("vidíš zde desítky obleků a různých doplňků některé jsou za přijatelnou cenu ale některé cenny sahají do astronomických výšin")
    pass

# print("hele mám tady pro tebe dva předměty nechám tě ale vybrat pouze jeden vybrat pamatuj že toto rozhodnutí ovlivní vývoj tvého příběhu")
 # oblečení a klub spadlé soudnosti
# print(" Nu dobrá přítely teď jsi už připravený abych tě pustil. Ndělej nic co by jsem udělal já")
# print("*vypravěč se otočí nasadí si čepici a odkráčí*\n")

# print("teď se můžeš rozhodnout jetsli půjdeš do obchodu s oblečením nebo do klubu který nese název klub spadlé soudnosti")





def predmesti():
    print("ocitnul jsi se na předměstí")
    input("press enter")

    odpoved = ""
    while odpoved not in ano_ne:
        odpoved = input("Stojíš před velkou cedulí s nápisem který nese jméno tohoto bohem zapomenutého místa BRIMINGHAM chceš vztoupit nebo ne?\nano/ne\n")
        if odpoved == "ano":
            print("Správná odpověď příteli jdeš přímo do samotného pekla.\n")
            pressKey()
            return namesti
        elif odpoved == "ne":
            print("Promiň ale nejsem tady jen tak pro zábavu. Sbohem, " + name + ".")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()
        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")
    return namesti

def namestiPribeh():
    print("""
Když jsi vyšel z obchodu
slyšel jsi rámus uprostřed ulice
zvedneš hlavu a vidíš tam alespoň 50 mužů a žen jak zmateně pobíhají 
kolem celého náměstí
ihned jak překročíš práh tak si tě našel tvůj přítel z brau a začne ti vysvětlovat událost
    """)
    pressKey()
    print("""
podívej naše rodina zde má takový né úplně legální dostihový sázky
a teď nám tam vtrhli chlupatí
to není žádná novinka ale ti šmejdi tam něco převrhli a začala nám hořet pracovna
teď se zde samozdřejmě seběhli lidi a delají povyk ale pro tebe mám práci bratříčku""")
    pressKey()
    print("""ty a tvůj přítel obcházíte shluk všech těch pobouřených občanů
zacházíte za hořící budovu

potom ti řekne: hele máme tady plno peněz musíš je pobrat bež tady tím zadním vchodem a vem co můžeš 
""")




    while odpoved not in ano_ne:
        odpoved = input("chceš se na něj vykašlat?\nano/ne\n")

        if odpoved == "ano":
            print("čekáš jak Thomas odejde a potom utíkáš co ti síly stačí dorazíš k bráně a zdrháš z tohoto podivnéo města\n")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "ne":
            print("vejdeš zadním vchodem do hořící budovy ")

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")


            print("""vybíháš schody až do předposeldního patra kde je napsáno Thomas S.R.O
hnedka jak otevřeš dveře tak cítíš ten žár do obličeje """)

    while odpoved not in ano_ne:
        odpoved = input("chceš tam vejít nebo ne?\nano/ne\n")

        if odpoved == "ano":
            print("""chvíli čekáš a pobíráš dech potom vlítneš do pokoje který hoří
a všude je už ohořelý nábytek ovšem jako zázrakem na konci pokoje je velký jídelní stůl
je plný peněz 
pobereš co můžeš 
+60000 šilinků
když se vracíš ke dveřím že od tam vypadneš vidíš jednoho ze zboru chlupatých jak stojí u dveří a už na tebe číhá
\n""")


        elif odpoved == "ne":
            print("""
otočíš se spátky a běžíš po schodec zase dolů ovšem po cestě
ovšem tam narazíš na jednoho ze zboru chlupatých ten vůbec neváhá a hned po tobě pálí z Revolveru""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")

    while odpoved not in utok:
        odpoved = input("""
ovšem zahodí Revolver a chce jít na pěstní souboj
kam ho udeříš?
krk/bricho/ruka\n""")

        if odpoved == "krk":
            print("""
bouchneš ho pěstí přímo do hrtanu nejdřív začne podivně vzdychat 
potom začíná sípat a asi za 5 sekund se svalí na zem a začíná lapad po dechu
ty to jako naprostý sociopat sleduješ 
náhle policajt modrá a poté se přestává hýbat""")


        elif odpoved == "bricho":
            print("""
když jsi ho praštil do břicha tak spadnul ale hned se svednul a už měl v ruce 
tipický policejní revolver a hned tě střelil
ty bohužel umíráš na prostřelení levé plíce""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "ruka":
            print("""
            Poté co jsi ho bouchnul do ruky tak se zasmál a bouchnul do hrudníku tak silně
že jsi cítil jak se ti zastavuje srdce
bohužel umíráš
            """)
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")

            print("""
hned jak jsi ho sundal tak běžíš po schodech rychle za Tomasem
jak jsi vyběhnul zadním vchodem zase ven tak vidíš hrozný obraz
Thomas leži na zemi a nad ním je skolení člen chlupatých a mlatí ho
i přez to že je Thomas už očividně nějakou dobu v bezvědomí
vedle nich stojí další dva policajti a přihlíží tomuto zvrácenému aktu""")

    while odpoved not in ano_ne:
        odpoved = input("budeš s nimi bojovat i přez to že je to docela riskantní?\nano/ne\n")

        if odpoved == "ano":
            print("""
ined zahazujš svůj kabát se všemy těmi penězi a běžíš mu na pomoc
""")


        elif odpoved == "ne":
            print("""
nenápadně je obejdeš a běžíš co ti síly stačí jak stojíš u hlavní brány
tak se akorát ohlídneš a řekneš si děkuji a sbohem BRIMINGHAME
a tak si odcházíš s plnými kapsamy dožít spokojený život někde na samotě""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")

    while odpoved not in taktika:
        odpoved = input("na kterého policajta zautočíš první?\nprvni/druhy\n")

        if odpoved == "prvni":
            prvni_polda()


        elif odpoved == "druhy":
            print("""
hned jak dobjehneš za druhým policajtem
(ten co mlátil Thomase)
tak se na tebe oba vrhnou
začnou tě mlátit 
mlátí tě tak dlouho dokud nejsi v bezvědomí
""")
            pressKey()
            print("""
potom najednou slyšíš neuvěřitelné prasknutí zní to odporně a nelidsky
trochu se ti podaří otevřít oči a vidíš že jsi celý potřísněný krví
            """)

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")

    while odpoved not in ano_ne:
        odpoved = input("budeš se chtít zvedat nebo ne?\nano/ne\n")

        if odpoved == "ano":
            print("""
snažíš se zvednout a slyšíš hlasy jak křičí
""")
            vzpominky()


        elif odpoved == "ne":
            print("""
zavíráš oči a potom se probouzíš někde v nemocnici a u tebe sedí Thomas
začne hned s nadšením vyprávět o tom jak jsi mu zachránil život atd.
ale ty jsi z toho slyšel jenom jednu věc
a to tu že kdyby jsi ho neposlechl a pohnul se tak by jsis udělal neco s páteří a mohl umřít
právě jsi uniknul smrti jen o vlásek říkal
""")
            pressKey()


            print("*2 roky minuly*")
            print(
"""hned jak jsi dostal propustku tak jsi vyrazil ven a šel za Thomasem na novou adresu
ten ti řekl že jsi si toho zažil za ten měsíc co jsi pro něj pracoval až až a teď ty dva roky 
zotavování no jé jé
takže ti dává 50000 šilinků a máš si je jít někam užít hlavně si nekupovat
žádné holky aby to nedopadlo jak minule
kulišácky se na tebe usměje a potom si už jen nasadí klobouk a stejně jako v baru pomalu odchazí s cigaretou v puse""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()




# funkce namesti
# jedna lokace, vraci dalsi lokaci ktera bude spustena v hlavni smycce hry
def namesti():

    if obchod_check and oblek_check and bar_check and Ammunation_check:
        namestiPribeh()

    print("ocitnul jsi se na náměstí")
    input("press enter")
    global penize

    odpoved = ""
    while odpoved not in smer:
         print("po tve leve straně vidíš obchod s potravinami")
         print("po tve pravici to stejné akorát to jsou obchody s oblečením")
         print("Přímo před tebou stojí velký obchod")
         print("a zatebou je východ ven\n")
         odpoved = input("Kudy se budeš chtít vydat?\nleva/prava/dopredu/zpatky\n")

         if odpoved == "leva":
             print("""
             Pomalu se stáčíš vlevo a přístupuješ k obchodu s potravinami.
             Při vstupu si všimneš vystaveného zboží, regály jsou plné 
             čerstvého zboží. 
             Uvědomíš si, že jsi celou dobu po cestě do města nic nejedl a máš velký hlad.             
             """)
             return potraviny

         elif odpoved == "prava":
             print("poté co se otočíš na svoji pravici vidíš tam obchod s oblečením a klub ")
             break

         elif odpoved == "dopredu":
             print("""vydal jsi se k obchodu který máš přímo před nosem
 najednou zasebou slyšíš hluk tak se ohlédneš a vidíš tam partu mladíků kteří přepadají jednoho starce
 než se stihneš zamyslet nad tím jestli mu pomoct nebo utéct
 tak si tě všimne jeden z mladíků a rozběhne se ktobě
 ty jsi ale tak v šoku že ze sebe zvládneš vyloudit akorát *uhhh*
 mladík je od tebe pouhé 2 kroky sáhne si do kabátu namíří na tebe Revolver Tula Nagant M.1895 a prostřelí ti hlavu""")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
             quit()

         elif odpoved == "zpatky":
             print("Hele " + name + " vidím že nejsi připravený na toto město tak se rozloučime.")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
             quit()

         else:
             print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

    print("mám tady pro tebe dva předměty než začneš tak jsem ti je chtěl dát ber to jako sponzorský dar")
    print("+50 šilinků")
    print("+vystřelovací nůž")

    inv.pridatDoInventare("nuz")
    inv.pridatDoInventare("penize")
    penize = penize + 50

    odpoved = ""

    while odpoved not in jaky_obchod:

         print("(pamatuj tvoje rozhodnutí ovlivní vývoj příběhu!)")
         print("-obchod s oblečením: decent but sexy")
         print("-místní jak se říká nalejvárna\n")
         odpoved = input("\nklub/obleceni co\nsi tedy vybereš: ")
         if odpoved == "klub":

            return klub

         elif odpoved == "obleceni":

             return obleceni

         else:
            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")


def prepadeni():
    print("hned na tebe začínají pořvávat nadávky v cizím jazyku")
    print("jediné co pochytíš je to aby jsi jim dal peníze")

    while odpoved not in ano_ne:

         odpoved = input("""dáš jim je nebo ne?
ano/ne""")
         if odpoved == "ano":
             print("""hned jak jim podáš svoje peníze tě střelí do hlavy""")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
             quit()

         elif odpoved == "ne":
             print("""rychle se otočíš a snažíš se zdrhnout ale ti parchanti vytáhnou 
Revolver a střelí tě nekolikrát do nohou
a potom si pro tobe dojdou okradou tě a nechají tě na místě""")

         else:
             print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")


    odpoved = ""

    while odpoved not in ano_ne:

         odpoved = input("\nklub/obleceni co\nsi tedy vybereš: ")
         if odpoved == "klub":
             return klub


         elif odpoved == "obleceni":
             return obleceni

         else:
             print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")




def zabitiChlapa():
    print("máš možnost ho zabít nožem či Revolverem který ti právě dal")
    odpoved = ""

    while odpoved not in nuz_revolver:

         print("kterou si tedy vybereš")
         odpoved = input("\nnuz/revolver\nsi tedy vybereš: ")
         if odpoved == "nuz":
             print("saháš si do kapsy")
             print("nahmatáváš tam nůž a peníze")
             print("hned jak ho pevně držíš tak s ním švihneš a bodáš ho muži opakovaně do krku")
             print("jeden z členů jeho zkupiny si tě všímá a postřelil tě ty se snažíš zdrhnout ale schytáváš další kulky a na následky umíráš")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
             quit()

         elif odpoved == "revolver":
             print("namíříš muži pistoli na hlavu ale nemáš žaludek na to spoušť zmáčknout on si toho hned všímá a střelí tě do břicha sebere ti zbraň a nechává tě vykrvácet")
             print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
             quit()
         else:
             print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

def klubPribeh():
    print("""mezitím co si piješ svoje pití tak slyšíš nějaký hluk zasebou
ohlédneš se a vidíš 2 muže ve středním věku *jeden má vousy a druhý ne*
jak se hádají o kus steaku 
oholený muž neustále zvyšuje hlas a začíná si sahat do kapsy 
vytahuje Revolver Webley Mark III. a míří sním na hlavu neoholenému muži
jak si toho všimnou ostatní muži vytahují pistole a hned se začíná stílet""")
    odpoved = ""

    while odpoved not in prestrelka_rozhodovani:
        print("kolem hlavy ti lítají kulky musíš se rychle rozhodnout co budeš dělat")
        print("snažit se zdrhnout")
        print("schovat se podstůl")

        odpoved = input("\nzdrhnout/schovka\n")
        if odpoved == "zdrhnout":
            print("""Rychle se zvedáš ze židle a utíkáš přímo ke dveřím
    saháš na kliku a otevíráš dveře
    najednou tě zasáhne kulka do hlavy""")
            print("*kulka tě trefila zezadu do hlavy, prorazila ti lebku a trefila tě do mozku je mi líto ale umřel si*")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "schovka":
            print("*Rychle seskočíš ze židle a převrhneš stůl*")
            print("zatím tě nikdo netrefil ")

        else:
            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")


    odpoved = ""
    while odpoved not in ano_ne:

        odpoved = input("dobrá co budeš dělat teď zůstaneš na místě?\nano/ne\n")

        if odpoved == "ano":
            print("zacpáváš s uši od všech těch výstřelů")
            print("čekáš minutu což trvá jako věčnost")
            print("""najednou slyšíš kroky za tvou hlavou
je mi líto ale někdo tě našel a zastřelil""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "ne":
            print("""dobře *postavíš se do dřepu*""")

        else:
            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

    odpoved = ""
    while odpoved not in prava_leva:
        odpoved = input("""nejbližší stůl za který se dá schovat je ten na pravo
na levo by si tě nikdo nemusel všimnout ovšem je to více riskantní možnost
kam se tedy vydáš- prava/leva""")

        if odpoved == "leva":

            print("""pomalu se šouráš kolem židle
kolem mrtvého těla a najednou za sebou slyšíš kroky
než se stihneš otočit tak cítíš bolest v zadu své hlavy""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "prava":
            print("pomalo se šouráš k dalšímu stolku")
            print("když najednou za sebou slyšíš kroky")
            print("""stíháš se ještě otočit""")
            print("vidíš tam docela urostlého muže jak ti míří na hlavu Revolverem")

        else:
            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

    odpoved = ""
    while odpoved not in ano_ne:
        odpoved = input("""ten ti říká: to se vážně hodláš plazit jak pes až ven?
opravdu nemáš ani kapku úcty
pravý muž by radši zemřel než se chovat jako ty
ihned se zvedej a pojď mi pomoct ty skunku
zvedneš se nebo ne?
ano/ne""")

        if odpoved == "ne":

            print("""muž se jenom pousměje a řekne:
nu dobrá synku jak myslíš ale viděl jsem v tobě docela budoucnost
namíří ti zbraní na hlavu a vystřelí""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "ano":
            print("dobrá to zní slibně ozve se mmuž")
            print("na zde máš pistol a střílej přesně")
            print("*muž ti podívá Revolver*")

        else:

            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

    odpoved = ""
    while odpoved not in ano_ne:
        odpoved = input("""chceš ho zabít nebo ne?
ano/ne""")

        if odpoved == "ne":

            print("""muž ti říka tak střílej nemáme na to celý den""")
            print("ty se napřahuješ a střílíš")

        elif odpoved == "ano":
            print("dobrá to zní slibně ozve se mmuž")
            print("na zde máš pistol a střílej přesně")
            print("*muž ti podívá Revolver*")
            zabitiChlapa()
            break

        else:
            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

    print("""asi po 20minutové přestřelce
se ozve znovu jeho hlas a říká: chkapče podívej zachránil jsi mi život chci se ti nějak odvděčit""")
    print("""co by jsi si teď nejvíc přál?
    nebo víš co jem me to jedno klidně si běž teď hned koupit ženu
    na tady máš peníze a kup si co chceš
    +150 šilinků""")
    global penize
    penize = penize + 150
    print("""muž tě ještě vyprovodí ven z klubu a řekne ti
jsi teď součást rodiny takže když tě nekdo bude otravovat
tak mě řekni a já ho spráskám jako psa""")

    odpoved = ""
    while odpoved not in ano_ne:
        odpoved = input("""co chceš teď dělat? 
koupit si lepší oblek a zbran nebo se jen tak se potulovat po ulicích
oblek/zbran/chill""")

        if odpoved == "oblek":

            print("jdeš do obchodu s oblečením")
            pressKey()
            return obleceni

        elif odpoved == "zbran":
            print("dopiješ svůj drink a vyrážíš")
            print("jdeš přez náměstí do ammunationu")
            print("*stojíš před ammunation*")
            pressKey()
            return Ammunition
        elif odpoved == "chill":
            print("""přemýšlíš že se bueš jen tak poflakovat
pomalu se ztrácíš ve svých myšlenkách když se ktobě najednou rozběhne parta parta mužů
""")
            return namesti
        else:
            print("promiň tomu nerozumím. Zkusíme to znovu dobře?\n")

def klub():
    bar_check = True

    print("ocitnul jsi se v klubu")
    input("press enter")
    pocet_vin = 0
    pocet_rum = 0
    odpoved = ""

    while odpoved not in barman_piti:

        # pokud si hráč dá 7x víno omdlí a zapne se random pokud padne 1 tak se zavolá se funkce clear a napíšeme mu že omdlel a někdo ho okradnul a pokud 2 tak ho neokradli
        # pokud si hráč dá 5x rum omdlí a vždycky ho okradou
        # pokud si hráč dá cokoliv kromě vody potom co ho okradli jenom nebo omdlel. Umírá na otravu alkoholem
        print("vidíš na tabuli:")
        print("víno 5šilinků")
        print("rum 18pencí)\n")        # přidává +5 život a zvyšuje šanci minout ránu o 1 na 4min (zapne se časovač)

        # přidává +40 život a zvyšuje šanci minout ránu o 3 na 4min (zapne se časovač)
        print("ochucenou vodu 2pence")

        odpoved = input("\nvino/rum/voda/\n")

        if odpoved == "vino":

            if placeni(5):
                print("""*barman ti podává víno 0,2 litrů* \n""")
                pocet_vin = pocet_vin + 1
                break
        elif odpoved == "rum":

            if placeni(18):
                print("*barman ti podává rum 0,2 litrů*")
                pocet_rum = pocet_rum + 1
        elif odpoved == "voda":
            if placeni(2):
                print("*braman ti podává vodu 0,5litrů*\n")


            odpoved = ""
        else:
            print("Barman: Hele, nedělej si ze mě prdel jasný. Řekni ti to Ještě jednou\n")

    global klub_prestrelka_probehla
    if not klub_prestrelka_probehla:
        klub_prestrelka_probehla = True
        return klubPribeh()
    else:
       print("dopil jsi drink a vyšel jsi na ulici")
       pressKey()
       return namesti


def obleceni():

    oblek_check = True

    print("ocitnul jsi se v obchodu s oblečením")
    input("press enter")
    print("vidíš zde desítky obleků a různých doplňků některé jsou za přijatelnou cenu ale některé cenny sahají do astronomických výšin")
    odpoved = None
    while odpoved not in ano_ne:
        odpoved = input("chceš si koupit oblek?\nano/ne\n")

        if odpoved == "ano":
            print("dobrá tedy\n")
            ano_oblek()

        elif odpoved == "ne":
            print("Tak co tady ještě děláš")
            return namesti

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")


def konec(zprava):
    print(zprava)
    input("press any key")
    return None

def ammunation():
    print("ocitnul jsi se v ammunation shopu")
    input("press enter")

    return namesti


# hlavní smyčka hry
lokace = uvod
while lokace != None:
    lokace = lokace()

def prvni_polda():
    print ("""
    hned se vrháš na toho prvního parchanta
jeho kumpáni jsou natolik zmatení že vůbec nestíhají zareagovat
použiješ zase svoji taktiku útok na krk kterou jsi využil už nahoře
""")

    while odpoved not in prvni_druhy:
        odpoved = input("na jakého zautočíš potom?\nprvni/druhy\n")

        if odpoved == "prvni":
            print("""nečekáš a hned útočíš bohužel teď si na tebe už došlápli
i přez to že jsi bestie napuštěná testosteronem
(jak se o tobě často v dobrém vtipkuje)
na dva muže už nestačíš a tak tě chytí a mlátí dokud tě nezabijí""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "druhy":
            print("""nečekáš a hned útočíš bohužel teď si na tebe už došlápli
i přez to že jsi bestie napuštěná testosteronem
(jak se o tobě často v dobrém vtipkuje)
na dva muže už nestačíš a tak tě chytí a mlátí dokud tě nezabijí""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        else:
            print("Promiň tomu nerozumím. Zkusíme to ještě jednou?\n")

def vzpominky():
    print("""vzbudíš se u sebe v pokoji ještě když jsi byl malí pišpunta
matka ti zrovna měří tepltu a říka ať se zbytečně nebudíš
nedokážeš ovšem rozeznat jestli je to sen nebo vzpomínka 
rozhlížíš se po pokoji a najednou tě hrozně rozbolí hlava""")
    while odpoved not in ano_ne:
        odpoved = input("chceš vstát z postele?\nano/ne\n")

        if odpoved == "ano":
            print("""
pomalu vstáváš z postele nasazuješ si bačkůrky 
jdeš svým malím dětským pokojem směrem do kuchyně 
matka na tebe začíná zvyšovat hlas ale ty ji nevnímáš
jediné co tě fascinuje je to podivné světlo keteré je na konci chodbyu dveří
jdeš pořád rovně a všímáš si že to prosvítá i kukátkem
koukně do něj a vidíš tam nějakého muže který je celý pokrytý krví otevíráš mu
a on ti říká že už teď bude vše vpořádku ať ho chytneš za ruku 
umíráš na poškození mozku""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()

        elif odpoved == "ne":
            print("""
ležíš dál ve své dětské postýlce dokud neslyšíš podivný zvuk z podposete
hned se tam koukneš když tu najednou
umíráš na poškození mozku""")
            print("𝓚𝓞𝓝𝓔𝓒 𝓗𝓡𝓨")
            quit()




