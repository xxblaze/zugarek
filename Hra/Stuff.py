class Inventar():

    def __init__(self, inventar = []):
        self.inv = inventar

    def oddelatZInventare(self, item):
        self.inv.remove(item)
        print("Tato položka byla úspěšně odstraněna z vašeho inventáře")

    def pridatDoInventare(self, item):
        if item in self.inv:
            print("tato položka se již nachází ve vašem inventáři")
        else:
            self.inv.append(item)
            print("položka byla úspěšně přidána do vašeho inventáře")

class Player:
    def __init__(self):
        self.zivot = 100
        self.poskozeni = 3
        self.inv = Inventar()
