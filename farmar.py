

pocetKralikuStr = input("prosim zadej pocet kraliku: ")
if not pocetKralikuStr.isnumeric():
    print("zapsaný text nebyl identifikován jako číslo")
    exit(1)
pocetKraliku = int(pocetKralikuStr)

pocetHusStr = input("prosim zadej pocet hus: ")
if not pocetHusStr.isnumeric():
    print("zapsaný text nebyl identifikován jako číslo")
    exit(1)

pocetHus = int(pocetHusStr)


pocetNohHusy = 2
pocetNohKralika = 4

pocetHlav = pocetHus + pocetKraliku

pocetNoh = pocetHus * pocetNohHusy + pocetKraliku * pocetNohKralika

print("\nPočet nohou: {}\nPočet hlav: {}".format(pocetNoh, pocetHlav))

pocetDnuRok = 365
kralikZradloDen = 0.5
husaZradloDen = 0.2

KralikZradloRok = kralikZradloDen * pocetDnuRok
husaZradloRok = husaZradloDen * pocetDnuRok

pocetKrmivaRok = KralikZradloRok + husaZradloRok


hektarMrkve = 50000
hektarPsenice = 4000


pocetZradlaKralikDen = pocetKraliku + kralikZradloDen
pocetZradlaKralikRok = pocetZradlaKralikDen * pocetDnuRok
potrebnaPlochaMrkev = pocetZradlaKralikRok / hektarMrkve
print("\nhmotnost krmiva na uživení králíků na 1 rok: {} kg".format(round(pocetZradlaKralikRok, 2)))

pocetZradlaHusaDen = pocetHus + husaZradloDen
pocetZradlaHusaRok = pocetZradlaHusaDen * pocetDnuRok
potrebnaPlochaPsenice = pocetZradlaHusaRok / hektarPsenice
print("\nhmotnost krmiva na uživení Hus na 1 rok: {} kg".format(round(pocetZradlaHusaRok, 2)))

celkovePole = potrebnaPlochaPsenice + potrebnaPlochaMrkev
print("\nPotřebná parcela pro uživení zvěřiny na jeden rok: {} hektarů".format(round(celkovePole, 2)))
